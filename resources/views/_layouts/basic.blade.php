<!DOCTYPE html>
<html lang="en" data-textdirection="LTR" class="loading">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="description" content="#">
  <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
  <meta name="author:Haidi Nurhadinata" content="#">
  <title>Dinas Pertambangan</title>
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-touch-fullscreen" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="default">
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  @include('_layouts.script-top')
 
  @yield('script-top')
    
</head>
<body>
  
  @include('_layouts.loading')

  @yield('basic-content')

  @include('_layouts.script-bottom')
  <script>
    var laravel = {
      csrfToken: '{{ csrf_token() }}'
    }
    var url = '{{ url("/") }}'
  </script>
  <script>
    $(document).ready(function() {
      $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': laravel.csrfToken
          }
      });
    });    
  </script>
  @yield('script-bottom')

</body>
</html>

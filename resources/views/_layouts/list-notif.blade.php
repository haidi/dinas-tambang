<li class="header-notification">
    <div class="dropdown-primary dropdown">
        <div class="dropdown-toggle" data-toggle="dropdown">
            <i class="menu-icon ti-cloud"></i>
            <span class="badge bg-c-pink">0</span>
        </div>
        <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn"
            data-dropdown-out="fadeOut">
            <li>
                <h6>Notifikasi</h6>
                {{-- <label class="label label-danger">New</label> --}}
            </li>
            <li>
                <div class="media">
                    <div class="media-body">
                        <p>Belum Ada</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</li>
<li class="header-notification">
    <div class="dropdown-primary dropdown">
        <div class="dropdown-toggle" data-toggle="dropdown">
            <i class="menu-icon ti-hand-point-up"></i>
            <span class="badge bg-c-pink">0</span>
        </div>
        <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn"
            data-dropdown-out="fadeOut">
            <li>
                <h6>Notifikasi</h6>
                {{-- <label class="label label-danger">New</label> --}}
            </li>
            <li>
                <div class="media">
                    <div class="media-body">
                        <p>Belum Ada</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</li>
<li class="header-notification">
    <div class="dropdown-primary dropdown">
        <div class="dropdown-toggle" data-toggle="dropdown">
            <i class="menu-icon ti-trash"></i>
            <span class="badge bg-c-pink">0</span>
        </div>
        <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn"
            data-dropdown-out="fadeOut">
            <li>
                <h6>Notifikasi</h6>
                {{-- <label class="label label-danger">New</label> --}}
            </li>
            <li>
                <div class="media">
                    <div class="media-body">
                        <p>Belum Ada</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</li>
<li class="header-notification">
    <div class="dropdown-primary dropdown">
        <div class="dropdown-toggle" data-toggle="dropdown">
            <i class="menu-icon ti-wheelchair"></i>
            <span class="badge bg-c-pink">0</span>
        </div>
        <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn"
            data-dropdown-out="fadeOut">
            <li>
                <h6>Notifikasi</h6>
                {{-- <label class="label label-danger">New</label> --}}
            </li>
            <li>
                <div class="media">
                    <div class="media-body">
                        <p>Belum Ada</p>
                    </div>
                </div>
            </li>
            {{-- <li>
                <div class="media">
                    <img class="d-flex align-self-center img-radius" src="https://vignette.wikia.nocookie.net/naruto/images/7/7b/Kurama2.png/revision/latest?cb=20140818171718" alt="Generic placeholder image">
                    <div class="media-body">
                        <h5 class="notification-user">John Doe</h5>
                        <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                        <span class="notification-time">30 minutes ago</span>
                    </div>
                </div>
            </li> --}}
        </ul>
    </div>
</li>
<li class="header-notification">
    <div class="dropdown-primary dropdown">
        <div class="dropdown-toggle" data-toggle="dropdown">
            <i class="menu-icon ti-folder"></i>
            <span class="badge bg-c-pink">0</span>
        </div>
        <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn"
            data-dropdown-out="fadeOut">
            <li>
                <h6>Notifikasi</h6>
                {{-- <label class="label label-danger">New</label> --}}
            </li>
            <li>
                <div class="media">
                    <div class="media-body">
                        <p>Belum Ada</p>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</li>
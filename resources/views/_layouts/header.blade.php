<nav class="pcoded-navbar">
    <div class="pcoded-inner-navbar main-menu">
        <br><br><br>
        {{-- <div class="pcoded-navigatio-lavel">Navigation</div> --}}
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon ti-server"></i></span>
                    <span class="pcoded-mtext">Data Kab/Kota</span>
                </a>
            </li>
            <li class="active">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon ti-server"></i></span>
                    <span class="pcoded-mtext">Data Perusahaan</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon ti-folder"></i></span>
                    <span class="pcoded-mtext">Jaminan Reklamasi</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon ti-user"></i></span>
                    <span class="pcoded-mtext">Kepala Teknik</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon ti-truck"></i></span>
                    <span class="pcoded-mtext">Izin Gudang Handak</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="fas fa-burn"></i></span>
                    <span class="pcoded-mtext">Kartu Izin Meledakkan</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="fas fa-radiation"></i></span>
                    <span class="pcoded-mtext">Izin Tangki BBC</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="fas fa-wrench"></i></span>
                    <span class="pcoded-mtext">Rekomendasi Teknis</span>
                </a>
            </li>
        </ul>
        <div class="pcoded-navigatio-lavel">Pelaporan</div>
        <ul class="pcoded-item pcoded-left-item">
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon fa fa-table"></i></span>
                    <span class="pcoded-mtext">Tahunan</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon fa fa-table"></i></span>
                    <span class="pcoded-mtext">Triwulan</span>
                </a>
            </li>
            <li class="">
                <a href="javascript:void(0)">
                    <span class="pcoded-micon"><i class="menu-icon fa fa-table"></i></span>
                    <span class="pcoded-mtext">Bulanan</span>
                </a>
            </li>
        </ul>
    </div>
</nav>
<nav class="navbar header-navbar pcoded-header">
    <div class="navbar-wrapper">
        <div class="navbar-logo">
            <a class="mobile-menu" id="mobile-collapse" href="#!">
                <i class="feather icon-menu"></i>
            </a>
            <a href="index.html">
                {{-- <img class="img-fluid" src="../files/assets/images/logo.png" alt="Theme-Logo" /> --}}
            </a>
            <a class="mobile-options">
                <i class="feather icon-more-horizontal"></i>
            </a>
        </div>

        <div class="navbar-container container-fluid">
            <ul class="nav-left">
                <li>
                    <a href="#!" onclick="javascript:toggleFullScreen()">
                        <i class="feather icon-maximize full-screen"></i>
                    </a>
                </li>
            </ul>
            <ul class="nav-right">
                @include('_layouts.list-notif')
                <li class="user-profile header-notification">
                    <div class="dropdown-primary dropdown">
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <img src="..\adminty\files\assets\images\avatar-4.jpg" class="img-radius"
                                alt="User-Profile-Image">

                            <span></span>
                            <i class="feather icon-chevron-down"></i>
                        </div>
                        <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn"
                            data-dropdown-out="fadeOut">
                            <li>
                                <a href="#!">
                                    <i class="feather icon-settings"></i> Settings
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    <i class="feather icon-log-out"></i>{{ __('Logout') }}
                                </a>
                                {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                    style="display: none;">
                                    @csrf
                                </form> --}}
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
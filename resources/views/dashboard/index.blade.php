@extends('_layouts.default')

@section('script-bottom')
    <script>
        function show_modal(ket)
        {
            // console.log(ket);
            $('h1').html(ket);

            $('#show-info').modal('show');
        }
    </script>
@endsection

@section('content')
@include('dashboard.modal')
<div class="page-header">
    <div class="row">
        <div class="col-md-8">
            <div class="page-header-title">
                <div class="d-inline">
                    <h4>Data Perusahaan</h4>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            {{-- <div class="page-header-breadcrumb">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index.html"> <i class="feather icon-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="#!">Form Picker</a></li>
                </ul>
            </div> --}}
        </div>
    </div>
</div>
<div class="page-body">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            {!! session()->get('message') !!}
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h5 id="title-x">My Data Progress</h5>        
        </div>
        <div class="card-block">
            <div class="table-responsive">
                <div class="dt-responsive table-responsive">
                    <table id="res-config" class="table table-striped table-bordered nowrap">
                        <thead>
                            <tr>
                                <th>Perusahaan</th>
                                <th>Jamrek</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    
                                    CV. Bumi Etam Bebaya
                                    <br>
                                    Direktur : <b>Xxx</b><br>
                                    KTT : <b>Tidak Ada</b><br>
                                    Alamat Kantor : <b>Xxx</b><br>
                                    No IUP : <b>Xxx</b><br>
                                    Luas Konsesi : <b>200 HA</b><br>
                                    Tanggal Berlaku : <b>24 Desember 2018</b><br>
                                    Tanggal Berakhir : <b>15 Desember 2019</b><br>
                        
                                </td>
                                <td></td>
                                <td>
                                    <a 
                                        href="javascript:;" 
                                        class="btn btn-md btn-danger"
                                        onClick="show_modal('Perusahaan ini telah berakhir masa berlakunya.')"
                                    >
                                        <i class="fa fa-info" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Jembayan Muara Bara
                                    <br>
                                    Direktur : <b>xxxx</b><br>
                                    KTT : <b>Tidak Ada</b><br>
                                    Alamat Kantor : <b>xxxxx</b><br>
                                    No IUP : <b>xxxx</b><br>
                                    Luas Konsesi : <b>0 HA</b><br>
                                    Tanggal Berlaku : <b>02 Desember 2018</b><br>
                                    Tanggal Berakhir : <b>1 Januari 2020</b><br>
                                </td>
                                <td></td>
                                <td>
                                    <a 
                                        href="javascript:;" 
                                        class="btn btn-md btn-success"
                                        onClick="show_modal('Perusahaan ini masih berlaku.')"
                                    >
                                        <i class="fa fa-info" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Maju Jaya
                                    <br>
                                    Direktur : <b>xxxx</b><br>
                                    KTT : <b>Tidak Ada</b><br>
                                    Alamat Kantor : <b>xxxxx</b><br>
                                    No IUP : <b>xxxx</b><br>
                                    Luas Konsesi : <b>0 HA</b><br>
                                    Tanggal Berlaku : <b>02 Desember 2018</b><br>
                                    Tanggal Berakhir : <b>1 Januari 2020</b><br>
                                </td>
                                <td></td>
                                <td>
                                    <a 
                                        href="javascript:;" 
                                        class="btn btn-md btn-success"
                                        onClick="show_modal('Perusahaan ini masih berlaku.')"
                                    >
                                        <i class="fa fa-info" aria-hidden="true"></i>
                                    </a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection